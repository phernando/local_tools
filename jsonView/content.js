var collapsers, options, jsonObject, jsonSelector;

function placeCSS(css){
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');
	style.type = 'text/css';
	if (style.styleSheet){
	  style.styleSheet.cssText = css;
	} else {
	  style.appendChild(document.createTextNode(css));
	}
	head.appendChild(style);	
}

function displayUI(targetId, html) {
	
	var statusElement, toolboxElement, expandElement, reduceElement, viewSourceElement, optionsElement, content = "";
	content += html;
	target = document.getElementById(targetId);
	target.className  += "bcjson";
	
	placeCSS('.bcjson ul {list-style-type: none;padding: 0px;margin: 0px 0px 0px 26px;}');
	placeCSS('.bcjson li {position: relative;}');
	placeCSS('.bcjson .hoverable {transition: background-color .2s ease-out 0s;-webkit-transition: background-color .2s ease-out 0s;display: inline-block;}');
	placeCSS('.bcjson .hovered {transition-delay: .2s;-webkit-transition-delay: .2s;}');
	placeCSS('.bcjson .selected {outline-style: solid;outline-width: 1px;outline-style: dotted;}');
	placeCSS('.bcjson .collapsed>.collapsible {display: none;}');
	placeCSS('.bcjson .ellipsis {display: none;}');
	placeCSS('.bcjson .collapsed>.ellipsis {display: inherit;}');
	placeCSS('.bcjson .collapser {position: absolute;top: 1px;left: -1.5em;cursor: default;user-select: none;-webkit-user-select: none;}');

	placeCSS('.bcjson ul {list-style-type: none;padding: 0px;margin: 0px 0px 0px 26px;}');
	placeCSS('.bcjson li {position: relative;}');
	placeCSS('.bcjson .hoverable {transition: background-color .2s ease-out 0s;-webkit-transition: background-color .2s ease-out 0s;display: inline-block;}');
	placeCSS('.bcjson .hovered {transition-delay: .2s;-webkit-transition-delay: .2s;}');
	placeCSS('.bcjson .selected {outline-style: solid;outline-width: 1px;outline-style: dotted;}');
	placeCSS('.bcjson .collapsed>.collapsible {display: none;}');
	placeCSS('.bcjson .ellipsis {display: none;}');
	placeCSS('.bcjson .collapsed>.ellipsis {display: inherit;}');
	placeCSS('.bcjson .collapser {position: absolute;top: 1px;left: -1.5em;cursor: default;user-select: none;-webkit-user-select: none;}');
	placeCSS('.bcjson {white-space: pre;font-family: monospace;}');
	placeCSS('.bcjson .property {font-weight: bold;}');
	placeCSS('.bcjson .type-null {color: gray;}');
	placeCSS('.bcjson .type-boolean {color: firebrick;}');
	placeCSS('.bcjson .type-number {color: blue;}');
	placeCSS('.bcjson .type-string {color: green;}');
	placeCSS('.bcjson .callback-function {color: gray;}');
	placeCSS('.bcjson .collapser:after {content: "-";}');
	placeCSS('.bcjson .collapsed > .collapser:after {content: "+";}');
	placeCSS('.bcjson .ellipsis:after {content: "...";}');
	placeCSS('.bcjson .collapsible {margin-left: 2em;}');
	placeCSS('.bcjson .hoverable {padding-top: 1px;padding-bottom: 1px;padding-left: 2px;padding-right: 2px;border-radius: 2px;}');
	placeCSS('.bcjson .hovered {background-color: rgba(235, 238, 249, 1);}');
	placeCSS('.bcjson .collapser {padding-right: 6px;padding-left: 6px;}');
	
	
	
	target.innerHTML = content;
	collapsers = document.querySelectorAll("#json .collapsible .collapsible");
	expandElement = document.createElement("span");
	expandElement.title = "expand all";
	expandElement.innerText = "+";
	reduceElement = document.createElement("span");
	reduceElement.title = "reduce all";
	reduceElement.innerText = "-";
	document.body.addEventListener('click', ontoggle, false);
	document.body.addEventListener('mouseover', onmouseMove, false);
	document.body.addEventListener('click', onmouseClick, false);
	document.body.addEventListener('contextmenu', onContextMenu, false);
	expandElement.addEventListener('click', onexpand, false);
	reduceElement.addEventListener('click', onreduce, false);

	
}

function extractData(rawText) {
	var tokens, text = rawText.trim();

	function test(text) {
		return ((text.charAt(0) == "[" && text.charAt(text.length - 1) == "]") || (text.charAt(0) == "{" && text.charAt(text.length - 1) == "}"));
	}

	if (test(text))
		return {
			text : rawText,
			offset : 0
		};
	tokens = text.match(/^([^\s\(]*)\s*\(([\s\S]*)\)\s*;?$/);
	if (tokens && tokens[1] && tokens[2]) {
		if (test(tokens[2].trim()))
			return {
				fnName : tokens[1],
				text : tokens[2],
				offset : rawText.indexOf(tokens[2])
			};
	}
}

function processData(data) {
		var json;
	 if (typeof data === 'object' ){
		    json = data;
	   }else {
		   json = JSON.parse(data);
	   }
		return objectToHTML(json)
		
		
		
}

function ontoggle(event) {
	var collapsed, target = event.target;
	if (event.target.className == 'collapser') {
		collapsed = target.parentNode.getElementsByClassName('collapsible')[0];
		if (collapsed.parentNode.classList.contains("collapsed"))
			collapsed.parentNode.classList.remove("collapsed");
		else
			collapsed.parentNode.classList.add("collapsed");
	}
}

function onexpand() {
	Array.prototype.forEach.call(collapsers, function(collapsed) {
		if (collapsed.parentNode.classList.contains("collapsed"))
			collapsed.parentNode.classList.remove("collapsed");
	});
}

function onreduce() {
	Array.prototype.forEach.call(collapsers, function(collapsed) {
		if (!collapsed.parentNode.classList.contains("collapsed"))
			collapsed.parentNode.classList.add("collapsed");
	});
}

function getParentLI(element) {
	if (element.tagName != "LI")
		while (element && element.tagName != "LI")
			element = element.parentNode;
	if (element && element.tagName == "LI")
		return element;
}

var onmouseMove = (function() {
	var hoveredLI;

	function onmouseOut() {
		var statusElement = document.querySelector(".status");
		if (hoveredLI) {
			hoveredLI.firstChild.classList.remove("hovered");
			hoveredLI = null;
			statusElement.innerText = "";
			jsonSelector = [];
		}
	}

	return function(event) {
		if (event.isTrusted === false)
			return;
		var str = "", statusElement = document.querySelector(".status");
		element = getParentLI(event.target);
		if (element) {
			jsonSelector = [];
			if (hoveredLI)
				hoveredLI.firstChild.classList.remove("hovered");
			hoveredLI = element;
			element.firstChild.classList.add("hovered");
			do {
				if (element.parentNode.classList.contains("array")) {
					var index = [].indexOf.call(element.parentNode.children, element);
					str = "[" + index + "]" + str;
					jsonSelector.unshift(index);
				}
				if (element.parentNode.classList.contains("obj")) {
					var key = element.firstChild.firstChild.innerText;
					str = "." + key + str;
					jsonSelector.unshift(key);
				}
				element = element.parentNode.parentNode.parentNode;
			} while (element.tagName == "LI");
			if (str.charAt(0) == '.')
				str = str.substring(1);
			statusElement.innerText = str;
			return;
		}
		onmouseOut();
	};
})();

var selectedLI;

function onmouseClick() {
	if (selectedLI)
		selectedLI.firstChild.classList.remove("selected");
	selectedLI = getParentLI(event.target);
	if (selectedLI) {
		selectedLI.firstChild.classList.add("selected");
	}
}

function onContextMenu(ev) {
	if (ev.isTrusted === false)
		return;
	var currentLI, statusElement, selection = "", i, value;
	currentLI = getParentLI(event.target);
	statusElement = document.querySelector(".status");
	if (currentLI) {
		var value = jsonObject;
		jsonSelector.forEach(function(idx) {
			value = value[idx];
		});
	value : typeof value == "object" ? JSON.stringify(value) : value
		
	}
}

function doJson(data,target) {
			
			displayUI(target,processData(data));
		
	
}


function htmlEncode(t) {
	return t != null ? t.toString().replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;") : '';
}

function decorateWithSpan(value, className) {
	return '<span class="' + htmlEncode(className) + '">' + htmlEncode(value) + '</span>';
}

function valueToHTML(value) {
	var valueType = typeof value, output = "";
	if (value == null)
		output += decorateWithSpan("null", "type-null");
	else if (value && value.constructor == Array)
		output += arrayToHTML(value);
	else if (valueType == "object")
		output += objectToHTML(value);
	else if (valueType == "number")
		output += decorateWithSpan(value, "type-number");
	else if (valueType == "string")
		if (/^https?:\/\/[^\s]+$/.test(value))
			output += decorateWithSpan('"', "type-string") + '<a href="' + htmlEncode(value) + '">' + htmlEncode(value) + '</a>' + decorateWithSpan('"', "type-string");
		else
			output += decorateWithSpan('"' + value + '"', "type-string");
	else if (valueType == "boolean")
		output += decorateWithSpan(value, "type-boolean");

	return output;
}

function arrayToHTML(json) {
	var i, length, output = '<div class="collapser"></div>[<span class="ellipsis"></span><ul class="array collapsible">', hasContents = false;
	for (i = 0, length = json.length; i < length; i++) {
		hasContents = true;
		output += '<li><div class="hoverable">';
		output += valueToHTML(json[i]);
		if (i < length - 1)
			output += ',';
		output += '</div></li>';
	}
	output += '</ul>]';
	if (!hasContents)
		output = "[ ]";
	return output;
}

function objectToHTML(json) {
	var i, key, length, keys = Object.keys(json), output = '<div class="collapser"></div>{<span class="ellipsis"></span><ul class="obj collapsible">', hasContents = false;
	for (i = 0, length = keys.length; i < length; i++) {
		key = keys[i];
		hasContents = true;
		output += '<li><div class="hoverable">';
		output += '<span class="property">' + htmlEncode(key) + '</span>: ';
		output += valueToHTML(json[key]);
		if (i < length - 1)
			output += ',';
		output += '</div></li>';
	}
	output += '</ul>}';
	if (!hasContents)
		output = "{ }";
	return output;
}

function jsonToHTML(json, fnName) {
	var output = '';
	if (fnName)
		output += '<div class="callback-function">' + htmlEncode(fnName) + '(</div>';
	output += '<div id="json">';
	output += valueToHTML(json);
	output += '</div>';
	if (fnName)
		output += '<div class="callback-function">)</div>';
	return output;
}

